package ru.bav.whackazombie.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;


public class Zombie {

    public Sprite zombieSprite; // спрайт для отображения зомби
    public Vector2 position = new Vector2();// позиция зомби
    public float height, width; // размеры зомби
    public float scaleFactor; // коэффициент масштабирования зомби

    public enum State {GOINGUP, GOINGDOWN, UNDERGROUND, HIT}

     // определение состояний зомби
    public State state = State.GOINGUP; // текущее состояние зомби

    public float currentHeight = 0.0f; // текущая величина высоты зомби относительно ямы
    public float speed = 2f; // скорость зомби
    public float timeUnderGround = 0.0f; // время с тех пор как зомби спрятался под землю
    public float maxTimeUnderGround = 0.8f; // максимальное время, разрешенное зомби оставаться под землей
    public float stunTime = 0.1f; // общее время оглушения, в котором будет находиться зомби
    public float stunCounter = 0.0f; // время в которое зомби был оглушен
    public Sprite hitSprite; // sprite для отображения изображения удара

    public void render(SpriteBatch batch) {

        zombieSprite.draw(batch);
        if (state == State.HIT) {
            hitSprite.draw(batch);
        }
    }

    public void update() {
        switch (state) {
            case UNDERGROUND:
                if (timeUnderGround >= maxTimeUnderGround) {
                    state = State.GOINGUP;
                    timeUnderGround = 0.0f;
                } else {
                    timeUnderGround += Gdx.graphics.getDeltaTime();
                }
                break;
            // увеличиваем высоту до максимума, как только высота достигнет максимума - меняем состояние
            case GOINGUP:
                currentHeight += speed;
                if (currentHeight >= height) {
                    currentHeight = height;
                    state = State.GOINGDOWN;
                }
                break;
            // уменьшаем высоту до минимума(0), как только высота достигнет минимума - меняем состояние
            case GOINGDOWN:
                currentHeight -= speed;
                if (currentHeight <= 0.0) {
                    currentHeight = 0.0f;
                    state = State.UNDERGROUND;
                }
                break;
            case HIT:
                if (stunCounter >= stunTime) {
                    //отправляем зомби под землю
                    state = State.UNDERGROUND;
                    stunCounter = 0.0f;
                    currentHeight = 0.0f;
                    randomizeWaitTime();
                } else {
                    stunCounter += Gdx.graphics.getDeltaTime();
                }
                break;
        }
        // рисуем только часть изображения зомби. Зависит от высоты над ямой
        zombieSprite.setRegion(0, 0, (int) (width / scaleFactor), (int) (currentHeight / scaleFactor));
        zombieSprite.setSize(zombieSprite.getWidth(), currentHeight);
    }

    public void randomizeWaitTime() {
        maxTimeUnderGround = (float) Math.random() * 2f;
    }

    public boolean handleTouch(float touchX, float touchY) {
        if ((touchX >= position.x) && touchX <= (position.x + width) && (touchY >= position.y) && touchY <= (position.y + currentHeight)) {

            hitSprite.setPosition(position.x + width - (hitSprite.getWidth() / 2), position.y + currentHeight - (hitSprite.getHeight() / 2));
            state = State.HIT;
            return true;
        }
        return false;
    }

}


