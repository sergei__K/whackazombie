package ru.bav.whackazombie.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import ru.bav.whackazombie.gameobjects.Zombie;

public class GameManager {

    static Array<Zombie> zombies; // массив зомби
    static Texture zombieTexture; // текстура зомби
    static Texture backgroundTexture; // текстура фона
    static Sprite backgroundSprite; // спрайт заднего фона
    static Texture holeTexture; // текстура  ямы
    static Array<Sprite> holeSprites; // массив спрайтов ям
    private static float HOLE_RESIZE_FACTOR = 1100f;
    static Texture hitTexture; //текстура для удара

    public static void initialize(float width, float height) {

        zombies = new Array<Zombie>();
        zombieTexture = new Texture(Gdx.files.internal("zombie.png"));

        backgroundTexture = new Texture(Gdx.files.internal("background.png")); // загрузка текстуры
        backgroundSprite = new Sprite(backgroundTexture); // установка текстуры в спрайт

        backgroundSprite.setSize(width, height);// настройка размеров и позиции спрайта заднего фона
        backgroundSprite.setPosition(0, 0f);

        hitTexture = new Texture(Gdx.files.internal("hit.png"));

        holeTexture = new Texture(Gdx.files.internal("hole.png"));
        holeSprites = new Array<Sprite>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Sprite sprite = new Sprite(holeTexture);
                // размер ям
                sprite.setSize(sprite.getWidth() * (width / HOLE_RESIZE_FACTOR), sprite.getHeight() * (width / HOLE_RESIZE_FACTOR));
                // позиции ям
                sprite.setPosition(width * (j + 1) / 4f - sprite.getWidth() / 2, height * (i + 1) / 4.4f - sprite.getHeight());
                holeSprites.add(sprite);
            }
        }

        // создание зомби и добавление их в массив
        for (int i = 0; i < 9; i++) {
            zombies.add(new Zombie());
        }

        // позиций для зомби
        for (int i = 0; i < 9; i++) {
            Zombie zombie = zombies.get(i);
            Sprite sprite = holeSprites.get(i);
            // установка спрайта для зомби
            zombie.zombieSprite = new Sprite(zombieTexture);

            zombie.hitSprite = new Sprite(hitTexture);

            // установка размеров зомби
            float scaleFactor = width / 600f;
            zombie.scaleFactor = scaleFactor;
            zombie.width = zombie.zombieSprite.getWidth() * (scaleFactor);
            zombie.height = zombie.zombieSprite.getHeight() * (scaleFactor);
            zombie.zombieSprite.setSize(zombie.width, zombie.height);
            // установка позиции зомби
            zombie.position.x = ((sprite.getX() + (sprite.getX() + sprite.getWidth())) / 2 - (zombie.zombieSprite.getWidth() / 2));
            zombie.position.y = (sprite.getY() + sprite.getHeight() / 5f);
            zombie.zombieSprite.setPosition(zombie.position.x, zombie.position.y);

            zombie.hitSprite.setSize(zombie.width / 2f, zombie.height / 2f);

            zombie.randomizeWaitTime();
        }

    }

    public static void renderGame(SpriteBatch batch) {

        backgroundSprite.draw(batch);
        // отображение ям
        for (Sprite sprite : holeSprites)
            sprite.draw(batch);
        // отображение зомби
        for (Zombie zombie : zombies) {
            zombie.update();
            zombie.render(batch);
        }
    }

    public static void dispose() {
        zombieTexture.dispose();
        backgroundTexture.dispose();
        holeTexture.dispose();
        hitTexture.dispose();
    }

}